<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        Schema::defaultStringLength(191);
        setlocale(LC_MONETARY, 'en_AE');

        Relation::morphMap([
            'Unit' => 'App\Models\Unit',
            'Lease' => 'App\Models\Lease',
            'Contact' => 'App\Models\Contact',
        ]);

        $printQuery = env('APP_DEBUG_WITH_LOGS', false);
        if($printQuery){
             DB::listen(function ($query) {
                echo ($query->sql);
                print_r($query->bindings);
                echo $query->time;
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
