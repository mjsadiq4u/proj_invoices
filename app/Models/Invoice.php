<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function invoicedEntity()
    {
        return $this->morphTo();
    }

    public function entitySpecificAttributes()
    {
        return $this->hasMany('App\Models\InvoiceAttribute');
    }

    public function getStatusAttribute($value) {
        return ucfirst($value);
    }

    public function getInvoicedEntityTypeAttribute($value) {
        return ucfirst($value);
    }

    public function getInvoicedAmountAttribute($value) {
        return number_format($value, 2);
    }

}
