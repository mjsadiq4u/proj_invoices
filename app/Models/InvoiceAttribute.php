<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceAttribute extends Model
{
    public function fieldAtrribute()
    {
        return $this->belongsTo('App\Models\MetaDataAttributes', 'field_id');
    }
}
