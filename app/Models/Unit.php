<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    public function leases()
    {
        return $this->hasMany('App\Models\Lease');
    }

    public function getGuideRentalAmountAttribute($value) {
        return number_format($value);
    }

    public function invoices()
    {
        return $this->morphMany('App\Models\Invoice', 'invoiced_entity');
    }

}
