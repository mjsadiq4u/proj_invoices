<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lease extends Model
{
    //
    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }

    public function getStartDateAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function getEndDateAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function getRentAttribute($value) {
        return number_format($value);
    }

    public function invoices()
    {
        return $this->morphMany('App\Models\Invoice', 'invoiced_entity');
    }
}
