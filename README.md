Server Requirements:
    As per Laravel:
        PHP >= 7.1.3
        OpenSSL PHP Extension
        PDO PHP Extension
        Mbstring PHP Extension
        Tokenizer PHP Extension
        XML PHP Extension
        Ctype PHP Extension
        JSON PHP Extension
        BCMath PHP Extension

    Mysql >= 5.5.5

    Composer - https://getcomposer.org/

Step 1:
    Create a database for the project, preferred 'project_invoices'.

Step 2:
    Clone the repository to a folder :
        $ mkdir project_invoices
        $ cd project_invoices
        $ git clone git clone https://mjsadiq4u@bitbucket.org/mjsadiq4u/proj_invoices.git .

Step 3:
    Copy .env.example to .env

        $ cp .env.example .env
        $ php artisan key:generate

    Change the mysql configuration as required in your server/local.
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=project_invoices
        DB_USERNAME=
        DB_PASSWORD=

Step 4:
    Install the required packages using :
        $ composer install

Step 5:
    Do the database migration and seeding.
        $ php artisan migrate --seed

Step 6:
    Start the server:
        $ php artisan serve

Then open the url as given by the command response.
