<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeding the database');

        Eloquent::unguard();

        $this->call([
            MetaDataAttributesTableSeeder::class,
            ContactsTableSeeder::class,
            UnitsTableSeeder::class,
            LeasesTableSeeder::class,
            InvoicesTableSeeder::class,
            InvoiceAttributesTableSeeder::class,
        ]);

        Eloquent::reguard();
    }
}
