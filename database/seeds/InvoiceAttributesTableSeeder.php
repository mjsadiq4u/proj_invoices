<?php

use Illuminate\Database\Seeder;
use App\Models\InvoiceAttribute;
use App\Models\MetaDataAttributes;
use App\Models\Invoice;

class InvoiceAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [];
        $metaAttributes = MetaDataAttributes::all();
        $metaAttributesGrouped = $metaAttributes->groupBy('for_entity')->toArray();
        $all_invoices = Invoice::all();
        if($all_invoices){
            foreach ($all_invoices as $invoice) {
                if(isset($metaAttributesGrouped[strtolower($invoice->invoiced_entity_type)])){
                    $metaForEntity = $metaAttributesGrouped[strtolower($invoice->invoiced_entity_type)];
                    foreach($metaForEntity as $meta){
                        $attribute['invoice_id'] = $invoice->id;
                        $attribute['field_id'] = $meta['id'];
                        $attribute['field_value'] = rand(10000, 600000);
                        $attributes[] = $attribute;
                    }
                }
            }
        }else{
            $this->command->warning('Could not find any invoices');
        }

        if($attributes){
            InvoiceAttribute::insert($attributes);
        }
    }
}
