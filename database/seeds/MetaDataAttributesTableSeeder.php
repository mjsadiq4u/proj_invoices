<?php

use Illuminate\Database\Seeder;

use App\Models\MetaDataAttributes;

class MetaDataAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [
            ['id' => '1', 'field_name' => 'is_lease_verified', 'for_entity' => 'lease'],
            ['id' => '2', 'field_name' => 'is_unit_verified', 'for_entity' => 'unit'],
            ['id' => '3', 'field_name' => 'lease_additional_field', 'for_entity' => 'lease'],
            ['id' => '4', 'field_name' => 'unit_additional_field', 'for_entity' => 'unit'],
            ['id' => '5', 'field_name' => 'contact_received', 'for_entity' => 'contact'],
            ['id' => '6', 'field_name' => 'contact_payment_info', 'for_entity' => 'contact']
        ];

        foreach ($attributes as $attribute) {
            $record = MetaDataAttributes::find($attribute['id']);
            if ($record) {
                $record->field_name = $attribute['field_name'];
                $record->save();
            } else {
                MetaDataAttributes::create($attribute);
            }
        }
    }
}
