<?php

use Illuminate\Database\Seeder;

class LeasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Lease::class, 50)->create();
    }
}
