<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Invoice::class, function (Faker $faker) {
    $all_entities = ['lease', 'unit', 'contact'];
    $all_statuses = ['void', 'paid', 'draft', 'sent'];

    $entity = $all_entities[rand(0, count($all_entities) - 1)];
    $status = $all_statuses[rand(0, count($all_statuses) - 1)];
    
    return [
        'status' => $status,
        'invoice_type_id' => rand(0, 6),
        'invoiced_entity_type' => $entity,
        'invoiced_entity_id' => rand(1, 50),
        'invoiced_amount' => $faker->randomNumber(6),
        'due_date' => $faker->dateTime()
    ];
});
