<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Unit::class, function (Faker $faker) {
    return [
        'unit_no' => $faker->unique()->randomNumber(5),
        'guide_rental_amount' => $faker->randomNumber(6),
        'size' => $faker->randomNumber(4),
    ];
});
