<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Lease::class, function (Faker $faker) {
    return [
        'rent' => $faker->randomNumber(5),
        'start_date' => $faker->dateTime('now'),
        'end_date' => $faker->dateTime(),
        'unit_id' => rand(1, 50)
    ];
});
