<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/units', 'UnitController@index');
Route::get('/units/{unit_id}', 'UnitController@show')->name('unit.show');;

Route::get('/leases', 'LeaseController@index');
Route::get('/contacts', 'ContactController@index');
Route::get('/invoices', 'InvoiceController@index');
