@extends('layouts.app')
    @section('bodyClass') leases-page @endsection
@section('content')
    <h2>Leases</h2>
    <div class="container">
        <div class="text-right">
            Total No. of Leases : {{ $leases->total() }}
        </div>
        <ul class="list-group list-group-flush">
            @foreach ($leases as $lease)
                <li class="list-group-item row">
                    <div class="col-md-6">
                        Lease Id: {{ $lease->id }}<br />
                        Rent: AED {{ $lease->rent }}<br />
                        Start Date: {{ $lease->start_date }}<br />
                        End Date: {{ $lease->end_date }}<br />
                    </div>
                    <div class="col-md-6">
                        Unit Number: {{ $lease->unit->unit_no }}<br />
                        Unit Id: {{ $lease->unit->id }}<br />
                    </div>
                </li>
            @endforeach
        </ul>
        <br />
        {{ $leases->links() }}
    </div>
@endsection
