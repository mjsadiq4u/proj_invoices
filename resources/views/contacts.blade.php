@extends('layouts.app')
    @section('bodyClass') contacts-page @endsection
@section('content')
    <h2>Contacts</h2>
    <div class="container">
        <div class="text-right">
            Total No. of Contacts : {{ $contacts->total() }}
        </div>
        <ul class="list-group list-group-flush">
            @foreach ($contacts as $contact)
                <li class="list-group-item row">
                    <div class="col-md-6">
                        First Name: {{ $contact->first_name }}<br />
                        Last Name: {{ $contact->last_name }}<br />
                        Email: {{ $contact->email }}<br />
                    </div>
                </li>
            @endforeach
        </ul>
        <br />
        {{ $contacts->links() }}
    </div>
@endsection
