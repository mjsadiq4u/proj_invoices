@extends('layouts.app')
    @section('bodyClass') units-page @endsection
@section('content')
    <h2>Units</h2>
    <div class="container">
        <div class="text-right">
            Total No. of Units : {{ $units->total() }}
        </div>
        <ul class="list-group list-group-flush">
            @foreach ($units as $unit)
                <li class="list-group-item">
                    <div class="col-md-6">
                        Unit Id: {{ $unit->id }}<br />
                        Unit Number: {{ $unit->unit_no }}<br />
                        Size: {{ $unit->size }}<br />
                        Rent: AED {{ $unit->guide_rental_amount }}<br />
                        No. Of Invoices : {{$unit->invoices_count}}<br />
                        No. Of Leases : {{$unit->leases_count}}
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{ route('unit.show', ['unit_id' => $unit->id]) }}"><i class="fa fa-receipt"></i> View Property & Invoices Details</a>
                    </div>
                </li>
            @endforeach
        </ul>
        <br />
        {{ $units->links() }}
    </div>
@endsection
