@extends('layouts.app')
@section('bodyClass') landing-page @endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="links">
                <a href="/units"><span class="fa fa-home"></span> Units</a>
                <a href="/leases"><span class="fa fa-handshake"></span> Leases</a>
                <a href="/invoices"><span class="fa fa-receipt"></span> Invoices</a>
                <a href="/contacts"><span class="fa fa-user"></span> Contacts</a>
            </div>
        </div>
    </div>
@endsection
