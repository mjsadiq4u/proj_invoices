@extends('layouts.app')
    @section('bodyClass') unit-page @endsection
@section('content')
    <h2>Unit</h2>
    <div class="container">

        <ul class="list-group list-group-flush">
            <div class="col-md-6">
                Unit Id: {{ $unit->id }}<br />
                Unit Number: {{ $unit->unit_no }}<br />
                Size: {{ $unit->size }}<br />
                Rent: AED {{ $unit->guide_rental_amount }}<br />
            </div>
        </ul>
        <div class="d-flex p-2">Invoices</div>
            @if ($unit->invoices)
                <ul class="list-group list-group-flush">
                    @foreach ($unit->invoices as $invoice)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="col-md-12">
                                Invoice #: {{ $invoice->id }} <br />
                                Status : {{ $invoice->status }} <br />
                                Amount : AED {{ $invoice->invoiced_amount }} <br />
                                Due Date : {{ $invoice->due_date }} <br />
                                @if ($invoice->entitySpecificAttributes)
                                    <strong>{{ $invoice->invoiced_entity_type }} Specific Invoice Data</strong>
                                    <ul>
                                        @foreach ($invoice->entitySpecificAttributes as $additionalAttribute)
                                            <li>{{ $additionalAttribute->fieldAtrribute->field_name }} : {{ $additionalAttribute->field_value }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </li>
                    @endforeach
                </ul>
            @else
                No Invoices Found.
            @endif
    </div>
@endsection
