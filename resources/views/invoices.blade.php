@extends('layouts.app')
    @section('bodyClass') contacts-page @endsection
@section('content')
    <h2>Invoices</h2>
    <div class="container">
        <div class="text-right">
            Total No. of Invoices : {{ $invoices->total() }}
        </div>
        <ul class="list-group list-group-flush">
            @foreach ($invoices as $invoice)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <div class="col-md-12">
                        Invoice #: {{ $invoice->id }} <br />
                        Status : {{ $invoice->status }} <br />
                        Amount : AED {{ $invoice->invoiced_amount }} <br />
                        Due Date : {{ $invoice->due_date }} <br />
                        Invoice For : {{ $invoice->invoiced_entity_type }}<br />
                        @if ($invoice->entitySpecificAttributes)
                        <strong>{{ $invoice->invoiced_entity_type }} Specific Invoice Data</strong> <br />
                            <ul>
                            @foreach ($invoice->entitySpecificAttributes as $additionalAttribute)
                                <li>{{ $additionalAttribute->fieldAtrribute->field_name }} : {{ $additionalAttribute->field_value }}</li>
                            @endforeach
                            </ul>
                        @endif
                        {{ $invoice->invoiced_entity_type }} Details :
                        <div class="col-md-8">
                            <ul>
                                @switch($invoice->invoiced_entity_type)
                                    @case('Unit')
                                        <li>Unit Id: {{ $invoice->invoicedEntity->id }}</li>
                                        <li>Unit Number: {{ $invoice->invoicedEntity->unit_no }}</li>
                                        <li>Size: {{ $invoice->invoicedEntity->size }}</li>
                                        <li>Rent: AED {{ $invoice->invoicedEntity->guide_rental_amount }}</li>
                                        @break

                                    @case('Lease')
                                        <li>Lease Id: {{ $invoice->invoicedEntity->id }}</li>
                                        <li>Rent: AED {{ $invoice->invoicedEntity->rent }}</li>
                                        <li>Start Date: {{ $invoice->invoicedEntity->start_date }}</li>
                                        <li>End Date: {{ $invoice->invoicedEntity->end_date }}</li>
                                        @break

                                    @default
                                        <li>Contact Id: {{ $invoice->invoicedEntity->id  }}</li>
                                        <li>First Name: {{ $invoice->invoicedEntity->first_name }}</li>
                                        <li>Last Name: {{ $invoice->invoicedEntity->last_name }}</li>
                                        <li>Email: {{ $invoice->invoicedEntity->email }}</li>
                                @endswitch
                            </ul>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
        <br />
        {{ $invoices->links() }}
    </div>
@endsection
